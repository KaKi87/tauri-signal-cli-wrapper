import { shell } from '@tauri-apps/api';

import signalCliWrapper from 'signal-cli-wrapper';

export default ({ signalCliPath }) => signalCliWrapper({
    execute: async args => {
        const { stdout, stderr } = await (new shell.Command(signalCliPath, args)).execute();
        return [stdout, stderr].join('\n').trim();
    },
    spawn: ({
        args,
        onData
    }) => {
        (async () => {
            const command = new shell.Command(signalCliPath, args);
            command.stdout.on('data', data => onData(data));
            command.stderr.on('data', data => onData(data));
            await command.spawn();
        })();
    }
});